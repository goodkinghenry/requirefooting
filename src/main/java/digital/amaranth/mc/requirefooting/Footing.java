package digital.amaranth.mc.requirefooting;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class Footing implements Listener {
    
    private final double STANDING_ON_GROUND_MAGIC = 0.001;  // the Y offset to seek below player for a ground block
    
    private boolean isStandingOnGround (Player p) {
        Block footingBlock = p.getLocation().subtract(0, STANDING_ON_GROUND_MAGIC ,0).getBlock();
        
        return footingBlock.getType().isSolid();
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockPlaceEvent (BlockPlaceEvent e) {
        Player p = e.getPlayer();

        if (!isStandingOnGround(p) && !p.isFlying()) {
            e.setCancelled(true);
        }
    }
}
