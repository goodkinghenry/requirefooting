package digital.amaranth.mc.requirefooting;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

import org.bukkit.plugin.java.JavaPlugin;

public class RequireFooting extends JavaPlugin {
    public static RequireFooting getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("RequireFooting");
        if (plugin == null || !(plugin instanceof RequireFooting)) {
            throw new RuntimeException("'RequireFooting' not found.");
        }
        
        return ((RequireFooting) plugin);
    }
    
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new Footing(), this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
