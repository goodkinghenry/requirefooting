# MC.RequireFooting

This plugin for Spigot API 1.19 servers requires players who are not flying to be standing on something in order to place a block. Basically, it nerfs dirt towers and similar cheap tricks.

Current version: 0.2
